package cn.display.pixelworksx7.constant;

import android.app.Activity;
import cn.display.pixelworksx7.MainActivity;
import cn.display.pixelworksx7.data.AppConfigInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GlobalData {
    public static AppConfigInfo appConfigInfo;

    public static Map<String, List<String>> irisConfigMap = new HashMap<>();// key 包名 value 参数集合

    public static String lastTopPackageName = "";// 上一次展示在上层的软件

    public static String lastConfigPackageName = ""; // 上次进行配置的软件

    Activity activity =new  MainActivity();
}
