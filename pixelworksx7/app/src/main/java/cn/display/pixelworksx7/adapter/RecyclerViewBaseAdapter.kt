package cn.display.pixelworksx7.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cn.display.pixelworksx7.R
import cn.display.pixelworksx7.data.AppConfigInfo

class RecyclerViewBaseAdapter(val appConfigInfoList:List<AppConfigInfo>) : RecyclerView.Adapter<RecyclerViewBaseAdapter.IrisConfigViewHolder>(){
    private lateinit var onItemClickListener: OnItemClickListener
    class IrisConfigViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        //获取子条目的布局控件ID
        val packageNameTextView: TextView = view.findViewById(R.id.packageName)
        val appNameTextView: TextView = view.findViewById(R.id.appName)
        val appIcon: ImageView = view.findViewById(R.id.appIcon)
        fun bindIrisConfig (appConfigInfo : AppConfigInfo) {
            packageNameTextView.text = appConfigInfo.packageName
            appNameTextView.text = appConfigInfo.appName
            appIcon.setImageDrawable(appConfigInfo.icon)
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewBaseAdapter.IrisConfigViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.iris_config_view, parent, false)
        return IrisConfigViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: RecyclerViewBaseAdapter.IrisConfigViewHolder, position: Int) {
        holder.bindIrisConfig(appConfigInfoList.get(position))
        var viewList: List<View> = listOf<View>(holder.itemView)
        for (item in viewList) {
            var view = item as View
            view.setOnClickListener {
                onItemClickListener?.onItemClick(view, position)
            }
            view.setOnLongClickListener {
                onItemClickListener?.onItemLongClick(view, position)
                true
            }
        }
//        holder.appNameTextView.setOnClickListener {
//            onItemClickListener?.onItemClick(holder.appNameTextView, position)
//        }
//        holder.appNameTextView.setOnLongClickListener {
//            onItemClickListener?.onItemLongClick(holder.appNameTextView, position)
//            true
//        }
    }

    override fun getItemCount(): Int {
        return appConfigInfoList.size
    }
    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.onItemClickListener = listener
    }
    interface OnItemClickListener{
        fun onItemClick(view: View, position: Int)
        fun onItemLongClick(view: View, position: Int)
    }
}

