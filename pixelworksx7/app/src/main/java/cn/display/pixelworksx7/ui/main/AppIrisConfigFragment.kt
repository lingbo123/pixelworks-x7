package cn.display.pixelworksx7.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import cn.display.pixelworksx7.MainActivity
import cn.display.pixelworksx7.R
import cn.display.pixelworksx7.constant.GlobalData
import cn.display.pixelworksx7.data.AppConfigInfoFile
import cn.display.pixelworksx7.databinding.AppIrisConfigFragmentBinding
import cn.display.pixelworksx7.util.IrisConfigUtil
import cn.display.pixelworksx7.util.ShellUtil
import kotlin.streams.toList

/**
 * A placeholder fragment containing a simple view.
 */
class AppIrisConfigFragment : Fragment() {
    // fragment是否已加载
    private var isPrepared = false
    private val tag: String = "AppIrisConfigFragment"
    private var _binding: AppIrisConfigFragmentBinding? = null
    var editTextList = mutableListOf<EditText>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        //可见的并且是初始化之后才加载

        var currentAppConfigInfo = GlobalData.appConfigInfo
        if (isPrepared && isVisibleToUser && currentAppConfigInfo != null) {
            if (currentAppConfigInfo.packageName!=GlobalData.lastConfigPackageName) {
                for (index in 0..editTextList.size-1) {
                    editTextList.get(index).setText("")
                }
            }
            var paramsList = currentAppConfigInfo.params
            if (paramsList != null) {
                editParamsText(paramsList)
            }
            binding.packageName.text = GlobalData.appConfigInfo!!.packageName
            binding.appName.text = GlobalData.appConfigInfo!!.appName
            binding.appIcon.setImageDrawable(GlobalData.appConfigInfo!!.icon)
        }
    }
    fun editParamsText (paramsList: List<String>) {
        if (paramsList != null) {
            for (index in 0..paramsList.size-1) {
                editTextList.get(index).setText(paramsList.get(index))
            }
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = AppIrisConfigFragmentBinding.inflate(inflater, container, false)
        editTextList.add(binding.paramsA)
        editTextList.add(binding.paramsB)
        editTextList.add(binding.paramsC)
        editTextList.add(binding.paramsD)
        val root = binding.root
        // 保存按钮
        binding.saveIrisConfig.setOnClickListener {
            var params: List<String> = listOf<String>(
                binding.paramsA.text.toString(),
                binding.paramsB.text.toString(),
                binding.paramsC.text.toString(),
                binding.paramsD.text.toString()
            )
            params = params.stream().filter { !it.trim().equals("") }.toList()
//            Log.e(tag,"传入前的参数--" + params.toString())
            setIrisConfigParam(AppConfigInfoFile(
                binding.packageName.text.toString(),
                binding.appName.text.toString(),
                params
            ))
        }
        // 视频模式
        binding.videoMode.setOnClickListener {
            var videoIrisConfigParamsList = resources.getStringArray(R.array.video_iris_config).toMutableList()
            editParamsText(videoIrisConfigParamsList)
            setIrisConfigParam(AppConfigInfoFile(
                binding.packageName.text.toString(),
                binding.appName.text.toString(),
                videoIrisConfigParamsList
            ))
        }
        //游戏模式
//        binding.gameMode.setOnClickListener {
//            var gameIrisConfigParamsList = resources.getStringArray(R.array.game_iris_config).toMutableList()
//            editParamsText(gameIrisConfigParamsList)
//            setIrisConfigParam(AppConfigInfoFile(
//                binding.packageName.text.toString(),
//                binding.appName.text.toString(),
//                gameIrisConfigParamsList
//            ))
//        }
        // 关闭轮询

        binding.closeCheckCurrentApp.setOnClickListener {
            MainActivity.noBreak=false
        }
        // 开启轮询
        binding.openCheckCurrentApp.setOnClickListener {
//            ShellUtil.toast(context,"开启轮询")
            MainActivity.noBreak=true
            MainActivity.openIrisConfigSet(true)
//            ShellUtil.toast(context,"开启轮询结果:" + MainActivity.noBreak)
        }
        isPrepared = true
//        val textView: TextView = binding.sectionLabel
        return root
    }
    fun setIrisConfigParam (appConfigInfoFile: AppConfigInfoFile) {
        var addIrisConfigResult = IrisConfigUtil.addIrisConfig(appConfigInfoFile)
        if (addIrisConfigResult>0) {
            ShellUtil.toast(context,"保存成功")
            IrisConfigUtil.loadIrisConfigFileParseToMap()
        }
        else if (addIrisConfigResult==0) {
            ShellUtil.toast(context,"保存失败")
        }
    }
    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): AppIrisConfigFragment {
            return AppIrisConfigFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
