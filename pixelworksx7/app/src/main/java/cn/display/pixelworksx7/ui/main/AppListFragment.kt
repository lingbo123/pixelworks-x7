package cn.display.pixelworksx7.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import cn.display.pixelworksx7.MainActivity
import cn.display.pixelworksx7.R
import cn.display.pixelworksx7.adapter.RecyclerViewBaseAdapter
import cn.display.pixelworksx7.constant.GlobalData
import cn.display.pixelworksx7.data.AppConfigInfo
import cn.display.pixelworksx7.databinding.AppListFragmentBinding
import cn.display.pixelworksx7.util.IrisConfigUtil
import cn.display.pixelworksx7.util.ShellUtil


class AppListFragment : Fragment() {
    private var _binding: AppListFragmentBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private var isPrepared = false
    private val tag:String = "AppListFragment"
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onStop() {
        super.onStop()
       // Log.w(tag,"onStop")
    }
    override fun onResume() {
        super.onResume()
        // Log.w(tag,"onResume")
    }
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser&&isPrepared) {
            val appListRecyclerView: RecyclerView = binding.appListRecyclerView
            appListRecyclerView.layoutManager = LinearLayoutManager(context)
            appListRecyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            var appInfoList = context?.let { IrisConfigUtil.getAppInfoList(it.packageManager) }
            var appConfigInfo = appInfoList?.get((0..appInfoList.size-1).random())
            if (appConfigInfo != null) {
                appConfigInfo.params = IrisConfigUtil.getIrisConfigByPackageName(appConfigInfo.packageName)
            }
            GlobalData.appConfigInfo = appConfigInfo
            val appListAdapter = appInfoList
                ?.let { RecyclerViewBaseAdapter(it) }
            appListAdapter?.setOnItemClickListener(object: RecyclerViewBaseAdapter.OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    if (appInfoList != null) {
                        var appConfigInfo = appInfoList.get(position)
                        appConfigInfo.params = IrisConfigUtil.getIrisConfigByPackageName(appConfigInfo.packageName)
                        GlobalData.appConfigInfo = appConfigInfo
                        var viewPager : ViewPager? = activity?.findViewById(R.id.view_pager)
                        if (viewPager != null) {
                            viewPager.currentItem = 1
                        }
                        //                    getActivity()?.supportFragmentManager
//                        ?.beginTransaction()
//                        ?.replace(R.id.view_pager, AppIrisConfigFragment.newInstance(position + 1))
//                        ?.addToBackStack(null)  //将当前fragment加入到返回栈中
//                        ?.commit();
                    }
                }
                override fun onItemLongClick(view: View, position: Int) {
                    if (appInfoList != null) {
                        ShellUtil.toast(context,appInfoList.get(position).packageName)
                    }
                }
            })
            appListRecyclerView.adapter = appListAdapter
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = AppListFragmentBinding.inflate(inflater, container, false)
        val root = binding.root
        val appListRecyclerView: RecyclerView = binding.appListRecyclerView
        appListRecyclerView.layoutManager = LinearLayoutManager(context)
        appListRecyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        var appInfoList = context?.let { IrisConfigUtil.getAppInfoList(it.packageManager) }
        var appConfigInfo = appInfoList?.get((0..appInfoList.size-1).random())
        if (appConfigInfo != null) {
            appConfigInfo.params = IrisConfigUtil.getIrisConfigByPackageName(appConfigInfo.packageName)
        }
        GlobalData.appConfigInfo = appConfigInfo
        val appListAdapter = appInfoList
            ?.let { RecyclerViewBaseAdapter(it) }
        appListAdapter?.setOnItemClickListener(object: RecyclerViewBaseAdapter.OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                if (appInfoList != null) {
                    var appConfigInfo = appInfoList.get(position)
                    appConfigInfo.params = IrisConfigUtil.getIrisConfigByPackageName(appConfigInfo.packageName)
                    GlobalData.appConfigInfo = appConfigInfo
                    var viewPager : ViewPager? = activity?.findViewById(R.id.view_pager)
                    if (viewPager != null) {
                        viewPager.currentItem = 1
                    }
                //                    getActivity()?.supportFragmentManager
//                        ?.beginTransaction()
//                        ?.replace(R.id.view_pager, AppIrisConfigFragment.newInstance(position + 1))
//                        ?.addToBackStack(null)  //将当前fragment加入到返回栈中
//                        ?.commit();
                }
            }
            override fun onItemLongClick(view: View, position: Int) {
                if (appInfoList != null) {
                    ShellUtil.toast(context,appInfoList.get(position).packageName)
                }
            }
        })
        appListRecyclerView.adapter = appListAdapter
        isPrepared = true
        return root
    }
    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"



        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): AppListFragment {
            return AppListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
