package cn.display.pixelworksx7.data

import android.graphics.drawable.Drawable

data class AppConfigInfo(val packageName: String, val appName:String, val icon:Drawable, var params: List<String>?)
data class AppConfigInfoFile(val packageName: String, val appName:String, val params: List<String>)
